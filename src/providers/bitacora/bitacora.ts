import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the BitacoraProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BitacoraProvider {

  URL = 'http://192.168.1.70:8888/Login/';

  constructor(private http: HttpClient) {

  }

  login(dataLogin:any)
  {

    const body = new HttpParams()
    .set('user', dataLogin.user)
    .set('pass', dataLogin.pass)

     return this.http.post(`${this.URL}Autentication.php`,body);   

  }

  logout(){

    const id = localStorage.getItem('user_id');

    const body = new HttpParams()
    .set('id', id);

    return this.http.post(`${this.URL}close.php`,body);  
  }

  download(){

    const id = localStorage.getItem('user_id');

    const body = new HttpParams()
    .set('id', id);

    return this.http.post(`${this.URL}download.php`,body);  
  }
}
