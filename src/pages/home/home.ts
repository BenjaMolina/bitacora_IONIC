import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BitacoraProvider } from '../../providers/bitacora/bitacora';

import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  loading:any;

  constructor(private navCtrl: NavController, private navParams: NavParams, private http:BitacoraProvider,private toastCtrl:ToastController, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
  }

  logout(){
    this.loading = this.loadingCtrl.create({
      content: 'Cerrando Sesion...',
      dismissOnPageChange: true
    })
    
    this.loading.present();


    this.http.logout().subscribe( 
      data => {
        console.log(data);

        if(data.logout){
          localStorage.setItem('user_id','');
          this.navCtrl.setRoot('LoginPage');
        }
      },
      error => {
        console.log(error.json());
        this.loading.dismiss();
      })
  }

  download()
  {
    this.http.download().subscribe(
      data => {
        console.log(data);
        if(data.status){
          const toast = this.toastCtrl.create({
            message: data.message,
            duration: 3000,
            position:'bottom'
          });
          toast.present();

        }
      },
      error => {
        console.log(error);
      }
    )
  }

}
