import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BitacoraProvider } from '../../providers/bitacora/bitacora';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  dataLogin = {
    'user':'',
    'pass':'',
  };

  loading:any;

  constructor(private navCtrl: NavController, 
              private bita:BitacoraProvider, 
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  homePage(page:string)
  {
    this.navCtrl.setRoot(page);
  }

  login()
  {
    console.log(this.dataLogin);

    this.loading = this.loadingCtrl.create({
      content: 'Iniciando Sesion...',
      dismissOnPageChange: true
    })
    
    this.loading.present();
    
    this.bita.login(this.dataLogin)
              .subscribe(res => {
                console.log(res);
                if(res.loggin){

                  this.navCtrl.setRoot('HomePage');

                  localStorage.setItem('user_id',res.id);
                }
                else{
                  
                  this.loading.dismiss();

                  const toast = this.toastCtrl.create({
                    message: res.message,
                    duration: 3000,
                    position:'bottom'
                  });
                  toast.present();
                }
              });

  }

 


}
